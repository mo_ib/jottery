package com.mohammad.jottery.services;

import com.mohammad.jottery.model.Prize;
import com.mohammad.jottery.model.Lottery;
import com.mohammad.jottery.model.Person;
import com.mohammad.jottery.repository.PrizeRepository;
import com.mohammad.jottery.repository.GradeRepository;
import com.mohammad.jottery.repository.LotteryRepository;
import com.mohammad.jottery.repository.PersonRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class LotteryService {
    private PrizeRepository prizeRepository;
    private GradeRepository gradeRepository;
    private PersonRepository personRepository;
    private LotteryRepository lotteryRepository;

    @Autowired
    public LotteryService(PrizeRepository prizeRepository, GradeRepository gradeRepository, PersonRepository personRepository, LotteryRepository lotteryRepository) {
        this.prizeRepository = prizeRepository;
        this.gradeRepository = gradeRepository;
        this.personRepository = personRepository;
        this.lotteryRepository = lotteryRepository;
    }

    public void lottery() {
        List<Person> personList = personRepository.findAll();
        List<String> chance = getChance(personList);
        Integer step = 1000;

        Collections.shuffle(personList);

        Integer chanceSize = chance.size();
        int randNumber = firstRandomNumber(chanceSize);
        List<Prize> prizeList = prizeRepository.findAll();

        prizeList.sort(new Comparator<Prize>() {
            @Override
            public int compare(Prize o1, Prize o2) {
                return o2.getGrade().getMinScore().compareTo(o1.getGrade().getMinScore());
            }
        });

        for (Prize prize : prizeList) {
            if (prize.getNumber() > 0) {
                for (int i=0;i<prize.getNumber();i++){
                    randNumber+=step;
                    if (randNumber>=chanceSize){
                        randNumber -=chanceSize;
                    }
                    String tmpCustomerCode = chance.get(randNumber);
                    if (!checkLot(tmpCustomerCode, prize)){
                        i--;
                    }
                }
            }
        }
    }

    private Boolean checkLot(String customerCode, Prize prize) {
        Person person = personRepository.findPersonByCode(customerCode);
        if (person.getScore() >= prize.getGrade().getMinScore() && !person.getWin()) {
            person.setWin(true);
            personRepository.save(person);
            Lottery lottery = new Lottery();
            lottery.setPrize(prize);
            lottery.setPersonCode(customerCode);
            lotteryRepository.save(lottery);
            prize.setNumber(prize.getNumber() - 1);
            return true;
        }
        return false;
    }

    private Integer firstRandomNumber(Integer range) {
        Integer leftLimit = 0;
        Integer rightLimit = range - 1;
        return (int) (leftLimit + (Math.random() * (rightLimit - leftLimit)));
    }

    public List<String> getChance(List<Person> people) {
        List<String> chance = new ArrayList<>();
        for (Person person : people) {
            for (int i = 0; i < person.getScore(); i++) {
                chance.add(person.getCode());
            }
        }
        return chance;
    }
}

