package com.mohammad.jottery.services;

import com.mohammad.jottery.model.Grade;
import com.mohammad.jottery.model.Prize;
import com.mohammad.jottery.model.PrizeDTO;
import com.mohammad.jottery.repository.PrizeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service @Slf4j
public class PrizeService {
    private PrizeRepository prizeRepository;

    @Autowired
    public PrizeService(PrizeRepository prizeRepository) {
        this.prizeRepository = prizeRepository;
    }

    public void addToPrizeRepository(Prize prize) {
        prizeRepository.save(prize);
    }

    public List<PrizeDTO> findAllPrize() {
        return toDto(prizeRepository.findAll());
    }

    List<PrizeDTO> toDto(List<Prize> prizeList){
        List<PrizeDTO> prizeDTOS = new ArrayList<>();
        prizeList.forEach(a->prizeDTOS.add(toDto(a)));
        return prizeDTOS;
    }
    PrizeDTO toDto(Prize prize){
        return new PrizeDTO(prize.getName(),prize.getNumber(),prize.getGrade().getName());
    }

    public void changePrize(String name, int number, Grade grade) {
        Prize prize = prizeRepository.findByName(name);
        prize.setName(name);
        prize.setNumber(number);
        prize.setGrade(grade);
        prizeRepository.save(prize);
    }

    public void addNewPrize(String name, int number, Grade grade) {
        Prize prize = new Prize(null, name, number, grade);
        prizeRepository.save(prize);
        log.info("Prize " + prize.getName() + " added to database.");
    }

}
