package com.mohammad.jottery.services;

import com.mohammad.jottery.model.Gender;
import com.mohammad.jottery.model.Person;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import java.io.File;
import java.util.Scanner;

@Component
@Getter
@Setter
public class Import {

    @Value("${person.csv.filename}")
    private String dataFileName;

    private Scanner csvReader;

    //private SimpleDateFormat dateFormat;

    @SneakyThrows
    public void init() {
        final File dataFile = new File(dataFileName);
        csvReader = new Scanner(dataFile);
        //dateFormat = new SimpleDateFormat("yyyy/MM/dd");
    }

    public Boolean hasNext(){
        return csvReader.hasNext();
    }

    public Person importPerson() throws PersonImportException{
        String[] row = new String[]{};
        try {
            final String rowString = csvReader.next();
            row = rowString.split(",");

            return new Person(null,row[0], Integer.parseInt(row[4]), row[2], row[3],row[1], row[7], Gender.valueOf(row[6]), null, false);
        }
        catch (Exception e){
                throw new PersonImportException(e, row);
        }
    }

    public static class PersonImportException extends Exception{
        public final String[] ROW;
        public PersonImportException(Exception e, String[] row){
            super(e);
            ROW=row;
        }

        @Override
        public String getMessage(){
            return "Error in person import!";
        }
    }
}