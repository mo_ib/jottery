package com.mohammad.jottery.services;

import com.mohammad.jottery.model.Grade;
import com.mohammad.jottery.model.GradeDTO;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.data.general.DefaultPieDataset;
import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class Chart {

    public void createChart(ArrayList<GradeDTO> gradeDTOs) {
        DefaultPieDataset dataset = new DefaultPieDataset();
        gradeDTOs.forEach(g -> dataset.setValue(g.getGradeName(), g.getCount()));

        JFreeChart chart = ChartFactory.createPieChart(
                "Customer Grades",   // chart title
                dataset,          // data
                true,             // include legend
                true,
                false);

        int width = 640;   /* Width of the image */
        int height = 480;  /* Height of the image */
        File pieChart = new File("PieChart.png");
        try {
            ChartUtilities.saveChartAsJPEG(pieChart, chart, width, height);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
