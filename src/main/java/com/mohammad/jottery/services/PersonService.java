package com.mohammad.jottery.services;

import com.mohammad.jottery.model.Person;
import com.mohammad.jottery.repository.PersonRepository;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service @Slf4j
public class PersonService {
    private PersonRepository personRepository;
    private Import anImport;
    private Validator validator;

    @Value("${person.auto.import.data}")
    private Boolean importData;
    @Value("${person.wipe.data}")
    private Boolean wipeData;
    @Value("${person.csv.filename}")
    private String filename;

    @Autowired
    public PersonService(PersonRepository personRepository, Import anImport, Validator validator) {
        this.personRepository = personRepository;
        this.anImport = anImport;
        this.validator = validator;
    }

    @PostConstruct
    public void init(){
        importPersons();
    }

    @SneakyThrows
    public void importPersons() {
        if (wipeData){
            personRepository.deleteAll();
        }
        if (importData) {
            anImport.init();

            Integer counter = 0;
            while (anImport.hasNext()) {
//                if (counter > 100) {
//                    return;
//                }
                try {
                    Person person = anImport.importPerson();
                    addPerson(person);
                } catch (Exception e) {
                    log.info(e.toString());
                }
                ++counter;
            }
        }
    }

    public Person addPerson(Person person){
        Person savedPerson = personRepository.save(person);
        if(validator.isValid(person)) {
            log.info(savedPerson.toString());
        }else{
            log.info("this person info is not valid by validator check : " + savedPerson.getCode());
        }
        return savedPerson;
    }

    public Long getTotalPersonCount(){
        return personRepository.count();
    }

    public Long findGradeRange(Integer start, Integer end) {
        return personRepository.rangeFinder(start, end);
    }

    public List<Person> findWinners() {
        return personRepository.findPersonByWinTrue();
    }
}