package com.mohammad.jottery.services;

import com.mohammad.jottery.model.Grade;
import com.mohammad.jottery.repository.GradeRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.List;

@Service @Slf4j
public class GradeService {
    private GradeRepository gradeRepository;

    @Autowired
    public GradeService(GradeRepository gradeRepository) {
        this.gradeRepository = gradeRepository;
    }

    @PostConstruct
    public void init(){
        setDefaultGrades();
    }

    public void setDefaultGrades(){
        Grade gradeA = new Grade(null,"A",1000,2000);
        gradeRepository.save(gradeA);
        Grade gradeB = new Grade(null,"B",600,999);
        gradeRepository.save(gradeB);
        Grade gradeC = new Grade(null,"C",20,599);
        gradeRepository.save(gradeC);
    }

    public void addToRepository(Grade grade) {
        gradeRepository.save(grade);
    }

    public Grade findGrade(String gradeName) {
        return gradeRepository.findByName(gradeName);
    }

    public void changeGrade(Integer minScore, Integer maxScore, String name){
        Grade grade = gradeRepository.findByName(name);
        grade.setMinScore(minScore);
        grade.setMaxScore(maxScore);
        gradeRepository.save(grade);
    }

    public void addNewGrade(Integer minScore, Integer maxScore, String name) {
        Grade grade = new Grade(null, name, minScore, maxScore);
        gradeRepository.save(grade);
        log.info("Grade " + grade.getName() + " added to database.");
    }

    public List<Grade> findAllGrades() {
        return gradeRepository.findAll();
    }
}
