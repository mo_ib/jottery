package com.mohammad.jottery.services;

import com.mohammad.jottery.model.Person;
import org.springframework.stereotype.Component;

@Component
public class Validator {
    public boolean isValid(Person person) {
        boolean valid = true;
        if (person.getCode().length()!=9){
            valid = false;
            System.out.println("Customer Number is not valid");
        }
        if (person.getNationalSecurityNumber().length()!=10){
            valid = false;
            System.out.println("Identity Number is not valid");
        }
        if (person.getPhoneNumber().length()==10 && person.getPhoneNumber().charAt(0)!='0') {
            person.setPhoneNumber("0"+person.getPhoneNumber());
        }
        if (person.getPhoneNumber().length()!=11){
            valid = false;
            System.out.println("Phone Number length is not valid");
        }
        if(person.getPhoneNumber().charAt(0)!= '0' || person.getPhoneNumber().charAt(1)!= '9'){
            valid = false;
            System.out.println("Phone Number 09 is not valid");
        }
        if(person.getScore() < 20){
            person.setScore(20);
        }
        if(person.getScore() > 2000){
            person.setScore(2000);
        }
        return valid;
    }
}