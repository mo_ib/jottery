package com.mohammad.jottery.services;

import com.mohammad.jottery.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@Service
public class ServiceFACADE {
    private PersonService personService;
    private LotteryService lotteryService;
    private GradeService gradeService;
    private PrizeService prizeService;

    @Autowired
    public ServiceFACADE(PersonService personService, LotteryService lotteryService, GradeService gradeService, PrizeService prizeService) {
        this.personService = personService;
        this.lotteryService = lotteryService;
        this.gradeService = gradeService;
        this.prizeService = prizeService;
    }

    @PostConstruct
    public void init(){
        setDefaultPrize();
    }

    private void setDefaultPrize() {
        Prize prize1 = new Prize(null,"MASERATI",1,gradeService.findGrade("A"));
        Prize prize2 = new Prize(null,"PORSCHE",2,gradeService.findGrade("B"));
        Prize prize3 = new Prize(null,"BMW",2,gradeService.findGrade("B"));
        Prize prize4 = new Prize(null,"DENA",10,gradeService.findGrade("C"));
        Prize prize5 = new Prize(null,"PRIDE",20,gradeService.findGrade("C"));
        prizeService.addToPrizeRepository(prize1);
        prizeService.addToPrizeRepository(prize2);
        prizeService.addToPrizeRepository(prize3);
        prizeService.addToPrizeRepository(prize4);
        prizeService.addToPrizeRepository(prize5);
    }

    public List<GradeDTO> showGradeList(){
        ArrayList<GradeDTO> gradeDTOList = new ArrayList<GradeDTO>();
        List<Grade> gradeList = gradeService.findAllGrades();
        //System.out.println(gradeList.get(0).getName());
        gradeList.forEach(g -> {
            System.out.println(gradeCount(g.getName()));
            GradeDTO gradeDTO = new GradeDTO(gradeCount(g.getName()),g.getName());
            gradeDTOList.add(gradeDTO);
        });
        GradeDTO total = new GradeDTO(personService.getTotalPersonCount(), "Total");
        gradeDTOList.add(total);
        return gradeDTOList;
    }

    public void chartDraw() {
        Chart demo = new Chart();
        ArrayList<Grade> gradeList = (ArrayList<Grade>) gradeService.findAllGrades();
        ArrayList<GradeDTO> gradeDTOs = new ArrayList<>();
        gradeList.forEach(g -> gradeDTOs.add(new GradeDTO(gradeCount(g.getName()), g.getName())));
        demo.createChart(gradeDTOs);
    }

    public long gradeCount(String name){
        return gradeCount(gradeService.findGrade(name).getMinScore(), gradeService.findGrade(name).getMaxScore());
    }

    public Long gradeCount(Integer start, Integer end){
        return personService.findGradeRange(start, end);
    }

    public void changeGrade(Integer start, Integer end, String name) {
        gradeService.changeGrade(start, end, name);
    }

    public void addNewGrade(Integer start, Integer end, String name) {
        gradeService.addNewGrade(start, end, name);
    }

    public List<PrizeDTO> showPrizeList() {
        return prizeService.findAllPrize();
    }

    public void changePrize(String name, int number, String gradeName) {
        Grade grade = gradeService.findGrade(gradeName);
        prizeService.changePrize(name, number, grade);
    }

    public void addNewPrize(String name, int number, String gradeName) {
        Grade grade = gradeService.findGrade(gradeName);
        prizeService.addNewPrize(name, number, grade);
    }

    public List<Person> lottery(){
        lotteryService.lottery();
        return personService.findWinners();
    }
}