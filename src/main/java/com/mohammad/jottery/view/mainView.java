package com.mohammad.jottery.view;

import com.mohammad.jottery.model.Person;
import com.mohammad.jottery.model.PrizeDTO;
import com.mohammad.jottery.model.GradeDTO;
import com.mohammad.jottery.services.ServiceFACADE;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.provider.DataProvider;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

@Route("")
public class mainView extends VerticalLayout {
    private ServiceFACADE serviceFACADE;
    Image image;

    @Autowired
    public void setPersonService(ServiceFACADE serviceFACADE){
        this.serviceFACADE = serviceFACADE;
    }

    public mainView(){
        image  = new Image("PieChart.png", "IMG");

        Label grade = new Label ("Grade: ");
        TextField start = new TextField();
        start.setPlaceholder("from");
        TextField end = new TextField();
        end.setPlaceholder("to");
        TextField name = new TextField();
        name.setPlaceholder("name");

        Button changeGrade = new Button ("Change Grade", event -> {
            changeGrade(start, end, name);
        });

        Button addGrade = new Button ("Add New Grade", event -> {
            addGrade(start, end, name);
        });

        Label prize = new Label ("Prize: ");
        TextField prizeName = new TextField();
        prizeName.setPlaceholder("Name");
        TextField prizeNumber = new TextField();
        prizeNumber.setPlaceholder("Number");
        TextField prizeGrade = new TextField();
        prizeGrade.setPlaceholder("Grade");

        Button changePrize = new Button ("Change Grade", event -> {
            changePrize(prizeName, prizeNumber, prizeGrade);
        });

        Button addPrize = new Button ("Add New Grade", event -> {
            addPrize(prizeName, prizeNumber, prizeGrade);
        });

        Grid<GradeDTO> grid1 = new Grid<>(GradeDTO.class);
        Button showGradeList = new Button ("Show Grade List", event -> {
            Notification.show( "Processing!" );
            grid1.setDataProvider(DataProvider.ofCollection(serviceFACADE.showGradeList()));
            serviceFACADE.chartDraw();
            image  = new Image("PieChart.png", "IMG");
        });

        Grid<PrizeDTO> grid2 = new Grid<>(PrizeDTO.class);
        Button showPrizeList = new Button ("Show Prize List", event -> {
            Notification.show( "Processing!" );
            grid2.setDataProvider(DataProvider.ofCollection(serviceFACADE.showPrizeList()));
        });

        Grid<Person> grid3 = new Grid<>(Person.class);
        Button showWinnerList = new Button ("Show Winner List", event -> {
            Notification.show( "Processing!" );
            grid3.setDataProvider(DataProvider.ofCollection(serviceFACADE.lottery()));
        });

        add(new H1("Jottery"),
                new HorizontalLayout( grade, start, end, name, changeGrade, addGrade),
                new HorizontalLayout( prize, prizeName, prizeNumber, prizeGrade, changePrize, addPrize),
                showGradeList,
                grid1,
                showPrizeList,
                grid2,
                image,
                showWinnerList,
                grid3
        );
    }

    private void changePrize(TextField prizeName, TextField prizeNumber, TextField prizeGrade) {
        if(!prizeName.getValue().equals("") && !prizeNumber.getValue().equals("") && !prizeGrade.getValue().equals("")) {
            serviceFACADE.changePrize(prizeName.getValue(), Integer.parseInt(prizeNumber.getValue()), prizeGrade.getValue());
            Notification.show("Prize Changed to: " + prizeName.getValue() + " - " + prizeNumber.getValue());
            prizeName.clear();
            prizeNumber.clear();
            prizeGrade.clear();
        }else if(prizeName.getValue().equals("") || prizeNumber.getValue().equals("") && prizeGrade.getValue().equals("")){
            Notification.show("Prize is: " + prizeName.getValue() + " - " + prizeNumber.getValue());
        }
    }
    private void addPrize(TextField prizeName, TextField prizeNumber, TextField prizeGrade) {
        if(!prizeName.getValue().equals("") && !prizeNumber.getValue().equals("") && !prizeGrade.getValue().equals("")) {
            serviceFACADE.addNewPrize(prizeName.getValue(), Integer.parseInt(prizeNumber.getValue()), prizeGrade.getValue());
            Notification.show("New Prize Added: " + prizeName.getValue() + " - " + prizeNumber.getValue());
            prizeName.clear();
            prizeNumber.clear();
            prizeGrade.clear();
        }else if(prizeName.getValue().equals("") || prizeNumber.getValue().equals("") && prizeGrade.getValue().equals("")){
            Notification.show("Prize is: " + prizeName.getValue() + " - " + prizeNumber.getValue());
        }
    }

    private void changeGrade(TextField start, TextField end, TextField name) {
        System.out.println(start.getValue());
        if(!start.getValue().equals("") && !end.getValue().equals("")) {
            serviceFACADE.changeGrade(Integer.parseInt(start.getValue()), Integer.parseInt(end.getValue()), name.getValue());
            Notification.show("Grade Changed to: " + start.getValue() + " - " + end.getValue());
            start.clear();
            end.clear();
            name.clear();
        }else if(start.getValue().equals("") || end.getValue().equals("")){
            Notification.show("Grade is: " + start.getValue() + " - " + end.getValue());
        }
    }
    private void addGrade(TextField start, TextField end, TextField name) {
        if(!start.getValue().equals("") && !end.getValue().equals("") && !name.getValue().equals("")) {
            serviceFACADE.addNewGrade(Integer.parseInt(start.getValue()), Integer.parseInt(end.getValue()), name.getValue());
            Notification.show("New Grade Added: "+ start.getValue() +" - "+ end.getValue());
            start.clear();
            end.clear();
            name.clear();
        }else if(start.getValue().equals("") || end.getValue().equals("") || name.getValue().equals("")){
            Notification.show("enter value to creat new grade");
        }
    }
}
