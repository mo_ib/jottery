package com.mohammad.jottery.repository;

import com.mohammad.jottery.model.Grade;
import com.mohammad.jottery.model.Prize;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PrizeRepository extends JpaRepository<Prize, Long> {
    Prize findByName(String name);
}
