package com.mohammad.jottery.repository;

import com.mohammad.jottery.model.GradeDTO;
import com.mohammad.jottery.model.Person;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
public interface PersonRepository extends JpaRepository<Person, Long> {

    Person findPersonByCode(String code);

    @Query(value = "select count(*) from Persons where score between ?1 and ?2", nativeQuery = true)
    Long rangeFinder(Integer start, Integer end);

    //@Query(value = "SELECT * FROM  Persons p WHERE p.win = 'TRUE''", nativeQuery = true)
    List<Person> findPersonByWinTrue();

//    List<Person> findFirst10ByCodeOOrderByLastNameDesc(String code);
//
//    List<Person> findByFirstName(String firstName);
//
//    @Query("select p from Person p where p.firstName = :firstName ")
//    List<Person> findByFirstName2(@Param("firstName") String firstName);
//
//    @Query(value = "select * from Person", nativeQuery = true)
//    List<Person> findAll2();
//
//    @Query(value = "SELECT new com.mohammad.jottery.model.GradeDTO(COUNT(p),p.grade.name) FROM Person p GROUP BY p.grade.id")
//    List<GradeDTO> countByGrade();
//
//    @Transactional @Modifying
//    @Query("UPDATE Person p SET p.grade.id= :id where p.score BETWEEN :minScore AND :maxScore")
//    void updateGrade(@Param("id") Long id,@Param("minScore") Integer minScore, @Param("maxScore") Integer maxScore);
}