//package com.mohammad.jottery.controller;
//
//import com.mohammad.jottery.model.Prize;
//import com.mohammad.jottery.repository.GradeRepository;
//import com.mohammad.jottery.repository.PrizeRepository;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.PostMapping;
//
//@Controller
//public class PrizeController {
//    private PrizeRepository prizeRepository;
//    private GradeRepository gradeRepository;
//
//    public PrizeController(PrizeRepository prizeRepository, GradeRepository gradeRepository) {
//        this.prizeRepository = prizeRepository;
//        this.gradeRepository = gradeRepository;
//    }
//
//    @GetMapping({"/Prize","/prize","/prizes"})
//    public String getMapping(Model model){
//        model.addAttribute(new Prize());
//        model.addAttribute("awards", prizeRepository.findAll());
//        model.addAttribute("levels", gradeRepository.findAll());
////        Iterable<LevelItem> levelItems = gradeRepository.findAll();
//        return "awards";
//    }
//
//    @PostMapping("/awardFormHandle")
//    public String postMapping(@ModelAttribute Prize prize){
//        prizeRepository.save(prize);
//        return "redirect:/prize";
//    }
//}
//
