//package com.mohammad.jottery.controller;
//
//import com.mohammad.jottery.model.Grade;
//import com.mohammad.jottery.repository.GradeRepository;
//import com.mohammad.jottery.repository.PersonRepository;
//import org.springframework.stereotype.Controller;
//import org.springframework.ui.Model;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.ModelAttribute;
//import org.springframework.web.bind.annotation.PostMapping;
//
//@Controller
//public class IndexController {
//    private PersonRepository personRepository;
//    private GradeRepository gradeRepository;
//
//    public IndexController(PersonRepository personRepository, GradeRepository gradeRepository) {
//        this.personRepository = personRepository;
//        this.gradeRepository = gradeRepository;
//    }
//
//    @GetMapping({"","/","/index","/home"})
//    public String getMapping(Model model){
//        model.addAttribute(new Grade());
//        model.addAttribute("levels", gradeRepository.findAll());
//        model.addAttribute("levelDTO",personRepository.countByGrade());
//        model.addAttribute("numberOfCustomers",personRepository.count());
//        return "index";
//    }
//
//    @PostMapping("/levelFormHandle")
//    public String postMapping(@ModelAttribute Grade grade){
//        gradeRepository.save(grade);
//        return "redirect:";
//    }
//
//    @PostMapping("/process")
//    public String process(){
//        Iterable<Grade> grades = gradeRepository.findAll();
//        for (Grade levelItem:grades){
//            personRepository.updateGrade(levelItem.getId(),(int) Math.round(levelItem.getMinScore()),(int) Math.round(levelItem.getMaxScore()));
//        }
//        return "redirect:";
//    }
//}
//
