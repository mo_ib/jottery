package com.mohammad.jottery.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Data @AllArgsConstructor @NoArgsConstructor
public class Lottery {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private String personCode;

    @ManyToOne
    private Prize prize;
}
