package com.mohammad.jottery.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Entity @ToString
@Data @AllArgsConstructor @NoArgsConstructor
public class Prize {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private String name;

    @NotNull @Positive
    private Integer number;

    @ManyToOne
    private Grade grade;
}
