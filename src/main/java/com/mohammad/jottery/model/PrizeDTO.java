package com.mohammad.jottery.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class PrizeDTO {
    private String name;
    private Integer number;
    private String gradeName;
}
