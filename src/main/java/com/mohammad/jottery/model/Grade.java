package com.mohammad.jottery.model;

import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import java.util.List;

@Entity @Table(name = "Grades")
@Data @AllArgsConstructor @NoArgsConstructor
public class Grade {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull
    private String name;

    @NotNull @Positive
    private Integer minScore;

    @NotNull @Positive
    private Integer maxScore;
}
