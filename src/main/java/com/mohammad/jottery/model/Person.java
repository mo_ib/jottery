package com.mohammad.jottery.model;

import lombok.*;
import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.Date;

@Entity @Table(name = "Persons")
@Data @AllArgsConstructor @NoArgsConstructor
public class Person {

    @Id @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @NotNull @Size(min = 9, max = 9)
    @Column(unique = true) @Basic(fetch = FetchType.EAGER)
    private String code;

    @NotNull @Positive
    private Integer score;

    @NotNull @Size(min = 1, max = 10)
    private String firstName;

    @NotNull @Size(min = 1, max = 10)
    private String lastName;

    @NotNull @Size(min = 10, max = 10) @Column(unique = true)
    private String nationalSecurityNumber;

    @NotNull @Size(min = 3, max = 12) @Basic(fetch = FetchType.LAZY)
    private String phoneNumber;

    @NotNull @Enumerated(EnumType.STRING)
    private Gender gender;

    @PastOrPresent
    private Date birthDate;

    @Column(nullable = false)
    private Boolean win = false;
}
