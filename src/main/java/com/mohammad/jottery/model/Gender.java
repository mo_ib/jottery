package com.mohammad.jottery.model;

public enum Gender {
    FEMALE, MALE, NOT_TO_SAY
}
