package com.mohammad.jottery;

import java.io.*;
import java.util.*;

public class TestReadFromAppProperties {
    public static void main(String[] args) throws IOException {
        FileReader fileReader = new FileReader("C:\\Users\\Mohammad\\IdeaProjects\\jottery\\src\\main\\resources\\application.properties");
        Properties properties = new Properties();
        properties.load(fileReader);
        System.out.println(properties.getProperty("CsvLocation"));
    }
}
